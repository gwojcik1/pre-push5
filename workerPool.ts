import { Worker } from "node:worker_threads";
import { TaskResultType, WorkerTaskType } from "./types";
import * as path from "node:path";
import { fileURLToPath } from "node:url";

class WorkerWrapper {
    private worker: Worker;
    private task: WorkerTaskType|undefined;
    private workerId: number;
    private taskSource: () => WorkerTaskType|undefined;
    constructor (workerId: number, taskSrc: () => WorkerTaskType|undefined, onFinish: (workerId:number) => void) {
        this.workerId = workerId;
        const dirName = path.dirname(fileURLToPath(import.meta.url));
        const workerFile = path.join(dirName, "WorkerMain.js");
        this.worker = new Worker(workerFile, {
            workerData: workerId,
        });
        this.taskSource = taskSrc;
        this.worker.on("message", (result : TaskResultType) => {
            if (this.task) {
                this.task.result = result;
            }
            const task = this.taskSource();
            if (!task) {
                onFinish(this.workerId);
            }
            this.runTaskInternal(task);
        });
    }
    private runTaskInternal(task : WorkerTaskType|undefined) {
        this.task = task;
        if (task) {
            this.worker.postMessage(task);
        }
    }
    runTask(task:WorkerTaskType) {
        if (this.task) {
            throw Error("Adding task while other is not finished");
        }
        this.runTaskInternal(task);
    }
    terminate() {
        this.worker.terminate();
    }
}

export class WorkerPool {
    private workers : WorkerWrapper[] = [];
    private tasks : WorkerTaskType[] = [];
    private taskFinished : Promise<void>|undefined;
    private taskFinishedResolve: ((value: void | PromiseLike<void>) => void)|undefined;
    private finishedWorkers : number = 0;
    private threadNum : number;
    constructor(threadNum: number) {
        this.threadNum = threadNum;
        this.finishedWorkers = threadNum;
        for (let i = 0; i < threadNum; i++) {
            this.workers.push(this.createWorker(i));
        }
    }

    addTasks(newTasks : WorkerTaskType[]) {
        if (this.tasks.length) {
            throw Error("Can't add tasks - prevoius task not finished");
        }
        this.taskFinished = new Promise((res) => {
            this.taskFinishedResolve = res;
        });
        this.tasks = newTasks;
        if (this.tasks.length) {
            this.runTasks();
        } else {
            this.finishPromise();
        }
    }

    private runTasks() {
        this.workers.forEach(w => {
            const task = this.tasks.pop();
            if (task) {
                this.finishedWorkers--;
                w.runTask(task);
            }
        });
    }

    getPromise() {
        if (!this.taskFinished) {
            throw Error("bad promise handling");
        }
        return this.taskFinished;
    }

    private finishPromise() {
        if (this.taskFinishedResolve) {
            this.taskFinishedResolve();
            this.taskFinishedResolve = undefined;
        } else {
            throw Error("bad promise handling");
        }
    }

    private createWorker(workerId:number) {
        let w = new WorkerWrapper(workerId, () => {
            return this.tasks.pop();
        }, (_workerId) => {
            this.finishedWorkers++;
            if (this.finishedWorkers == this.threadNum) {
                this.finishPromise();
            }
        });
        return w;
    }

    close() {
        this.workers.forEach(w => w.terminate());
    }
}
