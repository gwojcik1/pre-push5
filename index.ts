#!/usr/bin/env node
import { cpus } from "node:os";
import { getChangedFileContent, compareLogs } from "./git.js";

import { TestType, FileRulesType, FileDataType, WorkerTaskType, LogLine } from "./types.js";
import { WorkerPool } from "./workerPool.js";
import { getTests, initDefaultConfiguration } from "./configuration.js";
import { parseArgsAndInput } from "./parseInputs.js";

const tests: TestType[] = await getTests();

const params = await parseArgsAndInput();

function printHelp(): void {
    console.log("usage: TODO");
}

if (params.mode === "HELP") {
    printHelp();
    process.exit(0);
} else if (params.mode === "INVALID") {
    console.log(params.error);
    if (params.printUsage) {
        printHelp();
    }
    process.exit(1);
} else if (params.mode === "INIT") {
    await initDefaultConfiguration(params.force);
    process.exit(0);
} else {
    // TODO
}

// TODO user configurable thread number
const threadNum = cpus().length;

function getFileRulesFromTest(tests : TestType[]) : FileRulesType {
    let fileRegex_fileContent : string[] = [];
    outerLoop:
    for (let t of tests) {
        if (t.type === "COMPARE") {
            if (t.source.type === "FILE_CONTENT") {
                for (const e of t.source.extensions) {
                    if (e === ".*") {
                        fileRegex_fileContent = [".*"]
                        break outerLoop;
                    } else {
                        fileRegex_fileContent.push(`.*\\.${e}`);
                    }
                }
            }
        }
    }
    return {
        fileContent: new RegExp(fileRegex_fileContent.join("|")),
    };
}

const fileRules = getFileRulesFromTest(tests);

const fileData = await getChangedFileContent(
    params.localSha,
    params.remoteSha,
    fileRules
);

function genTasksForTests(fileData : FileDataType[], tests : TestType[]) {
    const tasks : WorkerTaskType[] = [];
    const taskGroups : WorkerTaskType[][] = [];
    for (const t of tests) {
        const fileRules = getFileRulesFromTest([t]);
        for (const file of fileData) {
            if (file.name.match(fileRules.fileContent)) {
                if (file.deleted) {
                    console.log(`skip test ${t.name} for deleted file ${file.name}`);
                    continue;
                }
                taskGroups.push([]);
                for (const content of [file.oldContent, file.newContent]) {
                    const task = {
                        content: {
                            name: file.name,
                            content: content,
                        },
                        test: t,
                    };
                    const workerTask: WorkerTaskType = {
                        type: "TEST",
                        data: task,
                        result: undefined,
                    };
                    taskGroups.at(-1)!.push(workerTask);
                    tasks.push(workerTask);
                }
            }
        }
    }
    return {tasks,taskGroups};
}

const {tasks: workerTasks, taskGroups} = genTasksForTests(fileData, tests);

const workerPool = new WorkerPool(threadNum);

// const workerTasks = tasks.map((x) : WorkerTaskType => ({type: "TEST", data: x, result: undefined}));

workerPool.addTasks(workerTasks);

await workerPool.getPromise();
workerPool.close();

async function processTaskGroups(taskGroups : WorkerTaskType[][]) {
    let results: LogLine[] = [];
    let isOk = true;
    for ( let group of taskGroups) {
        if (group.length === 2) {
            if (group[0].data.test.type === "COMPARE") {
                console.log(`[compare] test: "${group[0].data.test.name}", content: ${group[0].data.content.name}`);
                const newLog = group[1].result!.log;
                const oldLog = group[0].result!.log;
                const logFilter = group[0].result!.logFilter;
                const compareReslut = await compareLogs(newLog, oldLog, logFilter);
                if (compareReslut.length > 0) {
                    results.push({line: `${group[0].data.test.name}: ${group[0].data.content.name}`, type: 2});
                }
                results = results.concat(compareReslut);
                if (isOk) {
                    isOk = compareReslut.every(line => line.type === 0);
                }
            } else {
                throw Error("Unsupported test type in processing");
            }
        } else {
            throw Error("unsupported group size");
        }
    };
    return {results, isOk};
}

const {results: result, isOk} = await processTaskGroups(taskGroups);

writeResult(result);

function writeResult(result: LogLine[]): void {
    const red = process.stdout.isTTY ? "\x1b[1;31m" : "";
    const green = process.stdout.isTTY ? "\x1b[1;32m" : "";
    const reset = process.stdout.isTTY ? "\x1b[1;0m" : "";
    result.forEach(line => {
        if (line.type === 1) {
            console.log('NEW ' + red + line.line + reset);
        } else if (line.type === 2) {
            console.log(green + line.line + reset);
        } else {
            console.log('OLD ' + line.line);
        }
    });
}

process.exit(isOk ? 0 : 1);
