module.exports = {
    env: {
        browser: false,
        es2021: true
    },
    extends: 'standard-with-typescript',
    overrides: [
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
        project: ['./tsconfig.json']
    },
    rules: {
        indent: ['warn', 4],
        '@typescript-eslint/indent': ['warn', 4],
        quotes: "off",
        "@typescript-eslint/quotes": "off",
        "@typescript-eslint/no-unused-vars": "off",

        // TODO never semi?
        semi: ["warn", "always"],
        "@typescript-eslint/semi": ["warn", "always"],
    },
};
