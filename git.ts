import git, { DiffLine } from "nodegit";
import { FileDataType, FileRulesType, LogLine } from "./types";

import {Blob} from 'node:buffer';

// TODO configurable default encoding
const defaultFileEncoding = "utf8";

const repo = await git.Repository.open(".");

async function getTreeFromSpec(spec : string) {
    const obj = await git.Revparse.single(repo, spec);
    const commitId = obj.id();
    const commit = git.Commit.lookup(repo, commitId);
    return git.Tree.lookup(repo, (await commit).treeId());
}

export async function getChangedFiles(newCommitSpec : string, oldCommitSpec : string) {
    const newTree = await getTreeFromSpec(newCommitSpec);
    const oldTree = await getTreeFromSpec(oldCommitSpec);

    const diff = await git.Diff.treeToTree(repo, oldTree, newTree);

    const patches = await diff.patches();

    return patches.map( x => x.newFile().path());
}

export async function getChangedFileContent(newCommitSpec : string, oldCommitSpec : string, fileRules: FileRulesType) {
    const newTree = await getTreeFromSpec(newCommitSpec);
    const oldTree = await getTreeFromSpec(oldCommitSpec);

    // TODO diff options
    const diff = await git.Diff.treeToTree(repo, oldTree, newTree);

    const patches = await diff.patches();

    const filesToGet = patches.filter(x => fileRules.fileContent.test(x.newFile().path()))
        .map(x => [x.oldFile().path(), x.newFile().path()]);

    const resultFilesData : FileDataType[] = [];

    async function getContent(fileName : string, tree : git.Tree) {
        let entry;
        try {
            entry = await tree.entryByPath(fileName);
        } catch {
            // ignore - file probably deleted
        }
        if (entry) {
            const obj = await <Promise<git.Object>> entry.toObject(repo);
            if (obj.type() === git.Object.TYPE.BLOB) {
                const blobId = obj.id();
                const blob = await git.Blob.lookup(repo, blobId);
                const size = blob.rawsize();
                // TODO maybe use Buffer object?
                const content = blob.rawcontent().toBuffer(size).toString("utf8");
                return {
                    name: fileName,
                    content: content,
                    exist: true,
                };
            }
        }
        return {
            name: fileName,
            content: "",
            exist: false,
        };
    }

    for (const [oldFile, newFile] of filesToGet) {
        const oldContent = await getContent(oldFile, oldTree);
        const newContent = await getContent(newFile, newTree);
        resultFilesData.push({
            deleted: !newContent.exist,
            name: newContent.name,
            newContent: newContent.content,
            oldContent: oldContent.content,
        });
    }

    return resultFilesData;
}

async function getNewLineNos(newLog : string, oldLog : string) {
    let oldBlog = new Blob([oldLog]);
    let oldBuffer = Buffer.from(oldLog);
    let oldOid = await git.Blob.createFromBuffer(repo, oldBuffer, oldBlog.size);
    let oldBlob = await git.Blob.lookup(repo, oldOid);
    let newLines : number[] = [];
    function lineCb(_delta : git.DiffDelta, _hunk : any/*git.DiffHunk*/, line : DiffLine) {
        if (line.oldLineno() === -1) {
            let newLineNo = line.newLineno();
            newLines.push(newLineNo);
        }
    }
    await git.Diff.blobToBuffer(oldBlob, "/old", newLog, "/new", undefined, undefined, undefined, undefined,lineCb)
    return newLines;
}

export async function compareLogs(newLog: string, oldLog: string, logFilter: string|null) {
    const contextSize = 3;
    const newLines = newLog.split("\n");
    let lastAddedId = -1;
    let realAddded = false;

    const result: LogLine[] = [];

    const filteredNewLog = !!logFilter ? newLog.replace(RegExp(logFilter, 'gm'), '') : newLog;
    const filteredOldLog = !!logFilter ? oldLog.replace(RegExp(logFilter, 'gm'), '') : oldLog;

    (await getNewLineNos(filteredNewLog, filteredOldLog)).forEach(lineNo => {
        const lineId = lineNo - 1;
        if (realAddded && lastAddedId < lineId - contextSize) {
            const contextBound = lastAddedId + contextSize;
            while (lastAddedId < contextBound) {
                lastAddedId++;
                result.push({type: 0, line: newLines[lastAddedId]});
            }
        }
        if (lastAddedId < lineId - contextSize) {
            lastAddedId = Math.max(-1, lineId - contextSize - 1);
            if (lastAddedId >= 0) {
                result.push({type: 0, line: "..."});
            }
        }
        while (lastAddedId < lineId) {
            lastAddedId++;
            const isNewLine = lastAddedId === lineId;
            result.push({type: isNewLine ? 1 : 0, line: newLines[lastAddedId]});
        }
        realAddded = true;
    });

    return result;
}
