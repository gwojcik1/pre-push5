import { isMainThread, parentPort, workerData } from "node:worker_threads";
import {spawnSync} from "node:child_process";
import { TaskResultType, TaskType, WorkerTaskType } from "./types";
import * as path from "node:path";

if (isMainThread) {
    throw new Error("bad call of worker");
}
if (!parentPort) {
    throw new Error("bad call of worker");
}

const threadId = <number> workerData;

parentPort.on("message", (x:WorkerTaskType) => {
    if (x.type === "TEST") {
        const result = runTest(x.data);
        parentPort?.postMessage(result);
    } else {
        throw new Error("bad worker data");
    }
});

function runTest({content, test}: TaskType): TaskResultType {
    let textData = "";
    let resultText = "";
    const testedFileName = path.normalize(content.name);
    console.log(`[thread${threadId}]test: "${test.name}", content: ${testedFileName}`);
    if (test.source.type === "FILE_CONTENT") {
        textData = content.content;
    }
    let isOk = true;
    const testType = test.test.type;
    let logFilter = '';
    if (testType === "MATCH_BAD") {
        const testRegexStr = test.test.match;
        const testRegex = new RegExp(test.test.match);
        textData.split("\n").forEach((lineText, no) => {
            if (lineText.match(testRegex)) {
                resultText += `${testedFileName}:${no+1}: line match /${testRegexStr}/ "${lineText}"\n`;
                isOk = false;
            }
        });
        logFilter = ':\\d+:';
    } else if (testType === "SCRIPT") {
        const params = test.test.params.map(p => {
            if (typeof p === "string") {
                return p;
            } else {
                if (p[0] === "FILENAME") {
                    return content.name;
                } else {
                    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                    throw new Error(`unknown script paramteter type: "${p[0]}"`);
                }
            }
        });
        const result = spawnSync(test.test.executable, params, {
            input: content.content,
            shell: 'bash'
        });
        isOk = false;
        resultText = result.stdout.toString('utf-8');
        logFilter = test.test.logFilter;
    } else {
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        throw new Error(`unknown test type: "${testType}"`);
    }
    return {
        log: resultText,
        isOk,
        logFilter,
    };
}
