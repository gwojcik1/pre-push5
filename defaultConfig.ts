import { TestType } from "./types";

export const defaultTests: TestType[] = [{
    name: "bad word test",
    type: "COMPARE",
    source: {
        type: "FILE_CONTENT",
        extensions: ['*'],
    },
    test: {
        type: "MATCH_BAD",
        match: "TODO",
    }
}, {
    name: "eslint",
    type: "COMPARE",
    source: {
        type: "FILE_CONTENT",
        extensions: ['ts', 'js'],
    },
    test: {
        type: "SCRIPT",
        executable: "npx",
        params: ["eslint", "--no-color", "-f", "unix", "--stdin", "--stdin-filename", ["FILENAME"]],
        logFilter: ':\\d+:\\d+:|^\\d+ problems$',
    }
}];
