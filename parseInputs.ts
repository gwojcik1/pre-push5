export type ParamsType = {
    mode: "SINGLE"
    localSha: string
    remoteSha: string
} | {
    mode: "DOUBLE"
    localSha: string
    remoteSha: string
} | {
    mode: "HELP"
} | {
    mode: "INIT"
    force: boolean
} | {
    mode: "INVALID"
    printUsage: boolean
    error: string
} | {
    mode: "PRE_PUSH"
    remote: string
    url: string
    localRef: string
    localSha: string
    remoteRef: string
    remoteSha: string
    needMergeBase: boolean
};

async function readStdIn(): Promise<string> {
    const chunks = [];
    for await (const chunk of process.stdin) chunks.push(chunk);
    return Buffer.concat(chunks).toString("utf8");
}

async function preparePrePush(remote: string, url: string): Promise<ParamsType> {
    const input = await readStdIn();
    const lines = input.split('\n').filter(l => l.length !== 0);
    if (lines.length !== 1) {
        return {
            mode: "INVALID",
            error: "Unsupported pushing to multiple branches",
            printUsage: false,
        };
    }
    const [localRef, localSha, remoteRef, remoteSha] = lines[0].split(' ');
    const needMergeBase = /^0+$/.test(remoteSha);
    if (needMergeBase) {
        return {
            mode: "INVALID",
            error: "Unsupported pushing new branch",
            printUsage: false,
        };
    }
    return {
        mode: "PRE_PUSH",
        remote,
        url,
        localRef,
        localSha,
        remoteRef,
        remoteSha,
        needMergeBase,
    };
}

function fixCommitSpec(spec: string): string {
    if (spec.startsWith('@')) {
        return spec.replace('@', "HEAD");
    }
    return spec;
}

export async function parseArgsAndInput(): Promise<ParamsType> {
    const [_node, script, ...params] = process.argv;
    const scriptName = script.match(/.*[\\/](\S*?)$/)![1];
    if (scriptName === "pre-push") {
        return await preparePrePush(params[0], params[1]);
    }
    if (params[0] === '--pre-push') {
        if (params.length !== 3) {
            return {
                mode: "INVALID",
                error: "bad --pre-push parameters",
                printUsage: true,
            };
        }
        return await preparePrePush(params[1], params[2]);
    }
    if (params.includes('--help') || params.includes('-h')) {
        return {
            mode: "HELP",
        };
    }
    if (params[0] === "--init") {
        const force = params[1] === '--force';
        return {
            mode: "INIT",
            force,
        };
    }
    if (params.length === 1) {
        const spec = params[0];
        return {
            mode: "SINGLE",
            remoteSha: fixCommitSpec(spec),
            localSha: 'HEAD',
        };
    } else if (params.length === 2) {
        const oldSpec = params[0];
        const newSpec = params[1];
        return {
            mode: "DOUBLE",
            remoteSha: fixCommitSpec(oldSpec),
            localSha: fixCommitSpec(newSpec),
        };
    } else {
        return {
            mode: "INVALID",
            error: `invalid parameters: ${params.join(' ')}`,
            printUsage: true,
        };
    }
}
