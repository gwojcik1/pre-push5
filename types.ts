type TestSourceType = {
    type: "FILE_CONTENT",
    extensions: string[],
};

type scriptParameterTypes = "FILENAME";

type TestTestType = {
    type: "MATCH_BAD"
    match: string
} | {
    type: "SCRIPT"
    executable: string
    params: Array<string | [scriptParameterTypes]>
    logFilter: string
};

export type TestType = {
    name: string,
    type: "COMPARE",
    source: TestSourceType,
    test: TestTestType,
};

export type FileRulesType = {
    fileContent: RegExp,
};

export type FileDataType = {
    name: string,
    newContent: string,
    oldContent: string,
    deleted: boolean,
};

export type TaskContent = {
    name: string,
    content: string,
};

export type TaskType = {
    content: TaskContent,
    test: TestType,
};

export type TaskResultType = {
    log: string
    isOk: boolean
    logFilter: string
};

export type WorkerTaskType = {
    type: "TEST",
    data: TaskType,
    result: undefined|TaskResultType,
};

export type LogLine = {
    type: 0 | 1 | 2
    line: string
};
