import YAML from 'yaml';
import { defaultTests } from "./defaultConfig.js";
import { TestType } from './types.js';
import * as fs from "node:fs/promises";
import { constants } from "node:fs";

const fileName = "./pre-push5.yaml";

async function checkConfigFileExists(): Promise<boolean> {
    let fileExists;
    try {
        await fs.access(fileName, constants.F_OK);
        fileExists = true;
    } catch {
        fileExists = false;
    }
    return fileExists;
}

export async function getTests(): Promise<TestType[]> {
    const fileExists = await checkConfigFileExists();
    if (fileExists) {
        const fileHandle = await fs.open(fileName, 'r');
        const fileContent = await fileHandle.readFile({encoding: "utf8"});
        const allConfig = YAML.parse(fileContent) as {tests: TestType[]};
        await fileHandle.close();
        return allConfig.tests;
    } else {
        return defaultTests;
    }
}

export async function initDefaultConfiguration(force: boolean): Promise<void> {
    const fileExists = await checkConfigFileExists();
    if (fileExists && !force) {
        throw new Error("config file exists");
    }
    const fileHandle = await fs.open(fileName, 'w');
    await fileHandle.write(YAML.stringify({tests: defaultTests}));
    await fileHandle.close();
}
